use std::str;

use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder};
use hmac_sha256::HMAC;
use serde::{Deserialize, Serialize};

use configuration::get_configuration;

mod configuration;
mod twitter_events;

#[derive(Debug, Clone, Deserialize)]
struct CrcTokenRequest
{
    pub crc_token: String,
}

#[derive(Debug, Serialize)]
struct WebhookChallengeResponse
{
    pub response_token: String,
}

impl WebhookChallengeResponse
{
    fn new(token: String) -> Self
    {
        Self {
            response_token: format!("sha256={token}"),
        }
    }
}

#[get("/webhook")]
async fn webhook_challenge(query: web::Query<CrcTokenRequest>) -> HttpResponse
{
    let token = encode_webhook_challenge(query.crc_token.clone());

    let response = WebhookChallengeResponse::new(token);
    println!("{:?}", response);

    HttpResponse::Ok().json(response)
}

#[post("/webhook")]
async fn receive_webhook_event(event: web::Json<twitter_events::message::Message>) -> HttpResponse
{
    println!("Received an event");
    println!("{:#?}", event);

    HttpResponse::Ok().json("Event Received")
}

fn encode_webhook_challenge(crc: String) -> String
{
    let configuration = get_configuration().unwrap();

    let consumer_secret = configuration.twitter.secret.as_bytes();

    let mut hmac = HMAC::new(consumer_secret);
    hmac.update(crc);

    base64::encode(hmac.finalize())
}

#[get("/")]
async fn root() -> impl Responder
{
    HttpResponse::Ok().body("Kaibot root")
}

#[tokio::main]
async fn main() -> std::io::Result<()>
{
    HttpServer::new(|| {
        App::new()
            .wrap(actix_web::middleware::DefaultHeaders::new().add(("my-love", "Caryl :3")))
            .service(root)
            .service(
                web::scope("/twitter")
                    .service(webhook_challenge)
                    .service(receive_webhook_event),
            )
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}
