pub mod message
{
    use serde::Deserialize;

    #[derive(Debug, Deserialize)]
    pub struct Message
    {
        pub for_user_id: String,
        pub direct_message_events: Vec<message_components::MessageEvent>,
    }

    mod message_components
    {
        use serde::Deserialize;

        use crate::twitter_events::message::message_components::entities::Entities;

        #[derive(Debug, Deserialize)]
        pub struct MessageEvent
        {
            // TODO: Turn into enum
            pub r#type: String,
            pub id: String,
            pub created_timestamp: String,
            pub message_create: Option<message_create::MessageCreate>,
        }

        #[derive(Debug, Deserialize)]
        pub struct MessageData
        {
            pub text: String,
            pub entities: Entities,
        }

        mod entities
        {
            use serde::Deserialize;

            #[derive(Debug, Deserialize)]
            pub struct Entities
            {
                pub hashtags: Vec<Hashtag>,
                pub media: Vec<Media>,
            }

            #[derive(Debug, Deserialize)]
            pub struct Hashtag
            {
                pub indices: Vec<u16>,
                pub text: String,
            }

            #[derive(Debug, Deserialize)]
            pub struct Media
            {
                pub display_url: String,
                pub expanded_url: String,
                pub id: u64,
                pub id_str: String,
                pub indices: Vec<u16>,
                pub media_url: String,
                pub media_url_https: String,
                pub sizes: MediaSizes,
                // TODO: Turn into enum
                pub r#type: String,
                pub url: String,
            }

            #[derive(Debug, Deserialize)]
            pub struct MediaSizes
            {
                pub thumb: Option<Size>,
                pub large: Option<Size>,
                pub medium: Option<Size>,
                pub small: Option<Size>,
            }

            #[derive(Debug, Deserialize)]
            pub struct Size
            {
                pub h: u16,
                // TODO: Turn into enum
                pub resize: String,
                pub w: u16,
            }
        }

        mod message_create
        {
            use serde::Deserialize;

            use crate::twitter_events::message::message_components::MessageData;

            #[derive(Debug, Deserialize)]
            pub struct MessageCreate
            {
                pub target: Target,
                pub sender_id: String,
                pub source_app_id: String,
                pub message_data: MessageData,
            }

            #[derive(Debug, Deserialize)]
            pub struct Target
            {
                pub recipient_id: String,
            }
        }
    }
}
