use config::{Config, File};
use serde::{Deserialize, Serialize};

pub enum Environment
{
    Dev,
    Prod,
}

impl Environment
{
    pub fn as_str(&self) -> &'static str
    {
        match self
        {
            Environment::Dev => "dev",
            Environment::Prod => "prod",
        }
    }
}

impl TryFrom<String> for Environment
{
    type Error = String;

    fn try_from(s: String) -> Result<Self, Self::Error>
    {
        match s.to_lowercase().as_str()
        {
            "dev" => Ok(Self::Dev),
            "prod" => Ok(Self::Prod),
            other => Err(format!(
                "{} is not a supported environment. Use either `dev` or `prod`",
                other
            )),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Settings
{
    pub discord: DiscordSettings,
    pub twitter: TwitterSettings,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct DiscordSettings
{
    /// Discord app's application ID
    pub app_id: String,
    /// Discord bot's token
    pub discord_token: String,
    // TODO: The type of this should be `serenity::model::id::GuildId`
    /// The server ID
    // #[serde(deserialize_with = "deserialize_number_from_string")]
    pub guild_id: u64,
    /// Discord app's public key
    pub public_key: String,
}

/// All of the fields here are Twitter app-specific
#[derive(Debug, Serialize, Deserialize)]
pub struct TwitterSettings
{
    /// Twitter app key
    pub api_key: String,
    /// Twitter app secret
    pub secret: String,
    /// Twitter app bearer token
    pub bearer_token: String,
    pub client_id: String,
    pub client_secret: String,
}

pub fn get_configuration() -> Result<Settings, config::ConfigError>
{
    let base_path = std::env::current_dir().expect("Failed to determine the current directory");
    let configuration_directory = base_path.join("configuration");

    let environment: Environment = std::env::var("APP_ENVIRONMENT")
        .unwrap_or_else(|_| "dev".into())
        .try_into()
        .expect("Failed to parse APP_ENVIRONMENT");

    Config::builder()
        .add_source(config::File::from(configuration_directory.join("base")).required(true))
        .add_source(File::from(configuration_directory.join(environment.as_str())).required(true))
        .build()?
        .try_deserialize()
}
